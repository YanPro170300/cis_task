#include <arpa/inet.h>
#include <ctype.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/resource.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

static const size_t max_word_len = 255; // should be enough for almost all words

static const size_t vocabulary_size = 5; // why not?)

static const char* vocabulary[5] = {
    "one",
    "eleven",
    "phystech",
    "octopus",
    "computer"
};

volatile sig_atomic_t got_signal = 0;

void handle(int signal) {
    got_signal = 1;
}

enum request_type {
    SINGLE_LETTER,
    WHOLE_WORD,
    RESTART
};

// client/server information for epoll
struct agent_data {
    int fd;
    size_t try_count;
    size_t word_len;
    char* curr;
    char* word;
};

struct agent_data* init_server(char* ip, int port) {
    int server_fd = socket(AF_INET, SOCK_STREAM, 0);
    fcntl(server_fd, F_SETFL, O_NONBLOCK | fcntl(server_fd, F_GETFL));

    struct sockaddr_in server_addr = {AF_INET, htons(port), {inet_addr(ip)}};
    if (bind(server_fd, (void*)&server_addr, sizeof(server_addr)) != 0) {
        perror("bind");
        close(server_fd);
        return NULL;
    }

    struct agent_data* server_data = malloc(sizeof(struct agent_data));
    server_data->fd = server_fd;
    return server_data;
}

int init_epoll(int queue_size,
               struct agent_data* server_data,
               struct epoll_event** events) {
    int epoll_fd = epoll_create(queue_size);
    if (epoll_fd == -1) {
        perror("epoll_create");
        return -1;
    }

    struct epoll_event event;
    event.data.ptr = server_data;
    event.events = EPOLLIN | EPOLLHUP;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, server_data->fd, &event);

    *events = malloc(queue_size * sizeof(struct epoll_event));
    return epoll_fd;
}

size_t get_word(char** word) {
    int word_index = rand() % vocabulary_size;
    size_t word_len = strlen(vocabulary[word_index]);
    *word = malloc(word_len + 1);
    memcpy(*word, vocabulary[word_index], word_len + 1);
    return word_len;
}

void init_client(struct agent_data* client_data) {
    client_data->word_len = get_word(&client_data->word);
    client_data->curr = calloc(client_data->word_len, sizeof(char));
    memset(client_data->curr, '*', client_data->word_len);
    client_data->try_count = (3 * client_data->word_len) / 2;
    // let there be ~33% error
}

void accept_clients(int server_fd, int epoll_fd, int* client_count) {
    int client_fd = -1;
    struct epoll_event event;

    while (!got_signal && (client_fd = accept(server_fd, NULL, NULL)) != -1) {
        fcntl(client_fd, F_SETFL, O_NONBLOCK | fcntl(client_fd, F_GETFL));

        event.data.ptr = malloc(sizeof(struct agent_data));
        struct agent_data* client_data = event.data.ptr;
        init_client(client_data);
        client_data->fd = client_fd;
        event.events = EPOLLIN | EPOLLRDHUP;
        epoll_ctl(epoll_fd, EPOLL_CTL_ADD, client_fd, &event);

        ++*client_count;
    }
}

void proceed_client(struct agent_data* data) {
    char answer[max_word_len + 4]; // interaction protocol + '\0'
    memset(answer, 0, sizeof(answer));
    int answer_size = read(data->fd, answer, sizeof(answer));
    int word_guessed = 0;
    --data->try_count;

    if (answer[0] == SINGLE_LETTER) {
        word_guessed = 1;
        for (int i = 0; i < data->word_len; ++i) {
            if (data->word[i] == answer[1]) {
                data->curr[i] = answer[1];
            }
            if (data->curr[i] == '*') {
                word_guessed = 0;
            }
        }
    } else if (answer[0] == WHOLE_WORD) {
        if (memcmp(data->word, answer + 1, answer_size) == 0) {
            memcpy(data->curr, data->word, data->word_len);
            word_guessed = 1;
        }
    } else if (answer[0] == RESTART) {
        free(data->word);
        free(data->curr);
        init_client(data);
    }

    memset(answer, 0, sizeof(answer));
    answer[0] = word_guessed; // game_over
    answer[1] = data->try_count; // attempts left
    answer[2] = data->word_len; // word length
    memcpy(answer + 3, data->curr, data->word_len);
    
    write(data->fd, answer, data->word_len + 3);
}

void close_client_connection(struct agent_data* data,
                             int epoll_fd,
                             int* client_count) {
    epoll_ctl(epoll_fd, EPOLL_CTL_DEL, data->fd, NULL);
    shutdown(data->fd, SHUT_RDWR);
    close(data->fd);
    free(data->word);
    free(data->curr);
    free(data);
    --*client_count;
}

// argv[1] - IPv4 address, argv[2] - port
int main(int argc, char** argv) {
    srand(time(NULL));

    struct sigaction act = {.sa_handler = &handle, .sa_flags = SA_RESTART};
    sigaction(SIGTERM, &act, NULL); // let the players who already joined the server finish their game session
    sigaction(SIGINT, &act, NULL);  // no new clients would be accepted
    
    struct agent_data* server_data =
        init_server(argv[1], strtol(argv[2], NULL, 10));
    if (server_data == NULL) {
        exit(EXIT_FAILURE);
    }
    
    struct rlimit open_file_lim;
    if (getrlimit(RLIMIT_NOFILE, &open_file_lim) == -1) {
        close(server_data->fd);
        free(server_data);
        perror("getrlimit");
        exit(EXIT_FAILURE);
    }
    open_file_lim.rlim_cur = open_file_lim.rlim_max;
    int max_client_count = open_file_lim.rlim_max - 5; // stdin, stdout, stderr, server_fd, epoll_fd
    if (setrlimit(RLIMIT_NOFILE, &open_file_lim) == -1) {
        close(server_data->fd);
        free(server_data);
        perror("setrlimit");
        exit(EXIT_FAILURE);
    }
    struct epoll_event* events = NULL;
    int epoll_fd = init_epoll(max_client_count, server_data, &events);
    if (epoll_fd == -1) {
        close(server_data->fd);
        free(server_data);
        exit(EXIT_FAILURE);
    }

    if (listen(server_data->fd, SOMAXCONN) == -1) {
        close(server_data->fd);
        free(server_data);
        close(epoll_fd);
        free(events);
        perror("listen");
        exit(EXIT_FAILURE);
    }
    
    int client_count = 0;
    while (!got_signal || client_count > 0) {
        int event_count = epoll_wait(epoll_fd, events, max_client_count, -1);

        for (int i = 0; i < event_count; ++i) {
            struct agent_data* data = events[i].data.ptr;
            if (events[i].events & EPOLLIN) {
                if (data->fd == server_data->fd) {
                    accept_clients(server_data->fd, epoll_fd, &client_count);
                } else {
                    proceed_client(data);
                }
            }
            if (events[i].events & EPOLLRDHUP) {
                close_client_connection(data, epoll_fd, &client_count);
            }
        }
    }

    close(epoll_fd);
    free(events);
    shutdown(server_data->fd, SHUT_RDWR);
    close(server_data->fd);
    free(server_data);
    return 0;
}
